import { DateType } from '../components/macro/rover/rover.component';
import { Camera } from './camera';

export interface Bookmark {
  rover: string;
  dateType: DateType;
  selectedCamera: Camera;
  earthDate: string;
  solDate: number;
}
