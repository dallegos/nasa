import { PhotoManifest } from './manifests';
import { Photo } from './photo';
import { Rover } from './rover';

export interface RoverResult {
  rovers: Rover[];
}

export interface PhotoResult {
  photos: Photo[];
}

export interface ManifestResult {
  photo_manifest: PhotoManifest;
}
