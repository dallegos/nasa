import { Camera } from './camera';
import { PhotoManifest } from './manifests';
import { ManifestResult } from './result';

export interface Rover {
  id: number;
  name: string;
  slug: string;
  landing_date: string;
  launch_date: string;
  status: string;
  max_sol: number;
  max_date: string;
  total_photos: number;
  cameras: Camera[];
  manifest?: PhotoManifest;
}
