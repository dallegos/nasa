import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RoversComponent } from './components/macro/rovers/rovers.component';
import { NotFoundComponent } from './components/micro/not-found/not-found.component';
import { NotFoundPageComponent } from './components/macro/not-found-page/not-found-page.component';
import { RoverComponent } from './components/macro/rover/rover.component';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PhotoComponent } from './components/micro/photo/photo.component';
import { SpinnerComponent } from './components/micro/spinner/spinner.component';
import { GoTopButtonComponent } from './components/micro/go-top-button/go-top-button.component';
import { SearchingComponent } from './components/micro/searching/searching.component';
import { ToastrModule } from 'ngx-toastr';
import { BookmarkListComponent } from './components/micro/bookmark-list/bookmark-list.component';
import { BookmarkPipe } from './pipes/bookmark.pipe';

@NgModule({
  declarations: [
    AppComponent,
    RoversComponent,
    NotFoundComponent,
    NotFoundPageComponent,
    RoverComponent,
    PhotoComponent,
    SpinnerComponent,
    GoTopButtonComponent,
    SearchingComponent,
    BookmarkListComponent,
    BookmarkPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    InfiniteScrollModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
