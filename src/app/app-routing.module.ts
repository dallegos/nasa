import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundPageComponent } from './components/macro/not-found-page/not-found-page.component';
import { RoverComponent } from './components/macro/rover/rover.component';
import { RoversComponent } from './components/macro/rovers/rovers.component';

const routes: Routes = [
  {
    path: 'rovers',
    component: RoversComponent,
    data: { state: 'rovers' },
  },
  {
    path: 'rovers/:slug',
    component: RoverComponent,
    data: { state: 'rover' },
  },
  {
    path: '404',
    component: NotFoundPageComponent,
    data: { state: '404' },
  },
  { path: '', redirectTo: '/rovers', pathMatch: 'full' },
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
