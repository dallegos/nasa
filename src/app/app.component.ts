import { Component } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { scaleDownFromRight, fromTopEasing } from 'ngx-router-animations';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('rotateCube', [
      transition('* <=> 404', useAnimation(fromTopEasing)),
      transition('* <=> *', useAnimation(scaleDownFromRight)),
    ]),
  ],
})
export class AppComponent {
  public getState(outlet: any) {
    return outlet.activatedRouteData.state;
  }
}
