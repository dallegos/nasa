import { Pipe, PipeTransform } from '@angular/core';
import { Bookmark } from '../interfaces/bookmark';

@Pipe({
  name: 'bookmark',
})
export class BookmarkPipe implements PipeTransform {
  transform(bookmark: Bookmark): string {
    let string = '';

    let camera = Object.keys(bookmark.selectedCamera).length
      ? bookmark.selectedCamera.name
      : 'All';

    string = string.concat(`Camera: ${camera} - `);

    if (bookmark.dateType === 'sol') {
      string = string.concat(`Sol Date: ${bookmark.solDate}`);
    } else {
      const date = new Date(bookmark.earthDate);
      string = string.concat(`Earth Date: ${this.formatDate(date)}`);
    }

    return string;
  }

  private formatDate(date: Date): string {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }
}
