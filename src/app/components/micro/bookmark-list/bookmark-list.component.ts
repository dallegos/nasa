import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Bookmark } from 'src/app/interfaces/bookmark';

@Component({
  selector: 'app-bookmark-list',
  templateUrl: './bookmark-list.component.html',
  styleUrls: ['./bookmark-list.component.scss'],
})
export class BookmarkListComponent implements OnInit {
  @Input('bookmarks')
  bookmarks: Bookmark[] = [];

  @Output()
  emitter: EventEmitter<Bookmark> = new EventEmitter<Bookmark>();

  constructor() {}

  ngOnInit(): void {}

  setBookmark(bookmark: Bookmark) {
    this.emitter.emit(bookmark);
  }
}
