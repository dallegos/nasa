import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Photo } from 'src/app/interfaces/photo';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss'],
})
export class PhotoComponent implements OnInit {
  @Input('photo')
  photo: Photo = <Photo>{};

  @ViewChild('container', { static: false })
  container: ElementRef = <ElementRef>{};

  private _observer: IntersectionObserver = <IntersectionObserver>{};

  isPhotoLoaded: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    if (this.container) {
      this._observer = new IntersectionObserver(
        (
          entries: IntersectionObserverEntry[],
          observer: IntersectionObserver
        ) => {
          if (entries[0].isIntersecting && !this.isPhotoLoaded) {
            const img = entries[0].target.querySelector(
              'img'
            ) as HTMLImageElement;

            if (img) {
              const imgLoader = new Image();
              imgLoader.onload = () => {
                this.isPhotoLoaded = true;
                img.src = imgLoader.src;
                img.classList.add('shown');
              };
              imgLoader.src = this.photo.img_src;
              imgLoader.remove();
            }

            observer.disconnect();
          }
        },
        {
          root: null,
          rootMargin: '0px',
          threshold: 0.0,
        }
      );

      this._observer.observe(this.container.nativeElement);
    }
  }
}
