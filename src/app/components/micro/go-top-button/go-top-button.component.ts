import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-go-top-button',
  templateUrl: './go-top-button.component.html',
  styleUrls: ['./go-top-button.component.scss'],
})
export class GoTopButtonComponent implements OnInit {
  @ViewChild('goTopButton', { static: true })
  goTopButton: ElementRef = <ElementRef>{};

  isShown: boolean = false;

  constructor() {}

  ngOnInit(): void {
    this.goTopButton.nativeElement.addEventListener(
      'click',
      this._goTopFunction
    );
  }

  private _goTopFunction() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }

  @HostListener('window:scroll', ['$event']) getScrollHeight() {
    this.isShown = window.pageYOffset > 600;
  }
}
