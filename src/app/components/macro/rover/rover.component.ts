import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Camera } from 'src/app/interfaces/camera';
import { Photo } from 'src/app/interfaces/photo';
import { Rover } from 'src/app/interfaces/rover';
import { BookmarksService } from 'src/app/services/bookmarks.service';
import { RoversService } from 'src/app/services/rovers.service';
import { Bookmark } from 'src/app/interfaces/bookmark';

export type DateType = null | 'earth' | 'sol';

@Component({
  selector: 'app-rover',
  templateUrl: './rover.component.html',
  styleUrls: ['./rover.component.scss'],
})
export class RoverComponent implements OnInit, OnDestroy {
  componentDestroyed$: Subject<boolean> = new Subject();

  rover: Rover = <Rover>{};

  bookmarks: any = [];

  photos: Photo[] = [];
  selectedCamera: Camera = <Camera>{};

  dateType: DateType = null;
  earthDate: string = new Date().toString();
  solDate: number = 1;

  page: number = 1;
  hasMoreResults = true;
  resetPagination = true;

  isSearching: boolean = false;

  filtersOpened: boolean = false;

  constructor(
    private toastr: ToastrService,
    private roversService: RoversService,
    private bookmarksService: BookmarksService,
    private route: ActivatedRoute,
    private _elementRef: ElementRef,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.route.params
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((params) => {
        this.roversService.photos$
          .pipe(takeUntil(this.componentDestroyed$))
          .subscribe((photos) => {
            this.photos = photos;
          });

        this.roversService
          .getRover(params['slug'])
          .pipe(takeUntil(this.componentDestroyed$))
          .subscribe((rover) => {
            if (rover) {
              this.rover = rover;
              this.getPhotos();
            }
          });

        this.bookmarksService.bookmarks$
          .pipe(takeUntil(this.componentDestroyed$))
          .subscribe((bookmarks) => {
            this.bookmarks = bookmarks
              .filter((bookmark) => bookmark.rover == params['slug'])
              .reverse();
          });
      });
  }

  ngOnDestroy(): void {
    this.roversService.setPhotos([], false);

    this.dateType = null;
    this.earthDate = new Date().toISOString();
    this.solDate = 1;
    this.page = 1;
    this.hasMoreResults = true;

    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  getPhotos() {
    if (this.isSearching) return;

    this.isSearching = true;

    let searchObject: any = {
      rover: this.rover.slug,
      camera: this.selectedCamera.name,
      page: this.page,
      resetPagination: this.resetPagination,
    };

    if (this.dateType === 'earth' && this.earthDate) {
      searchObject.earth_date = this.earthDate;
    } else if (this.dateType === 'sol' && this.solDate) {
      searchObject.sol_date = this.solDate;
    }

    if (this.resetPagination) {
      this.roversService.setPhotos([], false);
    }

    this.roversService.getPhotosFromApi(searchObject).then((hasMoreResults) => {
      this.hasMoreResults = hasMoreResults;
      this.resetPagination = false;
      setTimeout(() => {
        this.isSearching = false;
      }, 1500);
    });
  }

  onScroll() {
    this.page++;
    if (this.hasMoreResults) this.getPhotos();
  }

  handleResetPaginationAndSearch() {
    this.resetPagination = true;
    this.hasMoreResults = true;
    this.page = 1;
    this.getPhotos();
  }

  handleSaveSearchParams() {
    const newBookmark: Bookmark = {
      rover: this.rover.slug,
      dateType: this.dateType,
      selectedCamera: this.selectedCamera,
      earthDate: this.earthDate,
      solDate: this.solDate,
    };

    if (!this.bookmarksService.setInfo(newBookmark)) {
      this.toastr.error(
        'An error has occurred when we tried to save the bookmark, please try again.',
        'Houston we have a problem!'
      );
    } else {
      this.toastr.success(
        'We receive the bookmark correctly.',
        'Bookmark has landed'
      );
    }
  }

  readBookmark(bookmark: Bookmark) {
    this.selectedCamera = bookmark.selectedCamera;
    this.dateType = bookmark.dateType || 'earth';
    this.solDate = bookmark.solDate;
    this.earthDate = this.formatDate(new Date(bookmark.earthDate));

    setTimeout(() => {
      const select = this._elementRef.nativeElement.querySelector(
        'input#earthDate'
      ) as HTMLSelectElement;

      if (select) {
        select.value = this.formatDate(new Date(bookmark.earthDate));
      }

      this.handleResetPaginationAndSearch();
    }, 100);
  }

  compareFunction(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  toggleFilterBox() {
    this.filtersOpened = !this.filtersOpened;
  }

  private formatDate(date: Date): string {
    return (
      date.getFullYear() +
      '-' +
      ('0' + (date.getMonth() + 1)).slice(-2) +
      '-' +
      ('0' + date.getDate()).slice(-2)
    );
  }
}
