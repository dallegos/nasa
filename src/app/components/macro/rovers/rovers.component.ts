import { Component, OnInit } from '@angular/core';
import { RoversService } from 'src/app/services/rovers.service';
import { Rover } from 'src/app/interfaces/rover';
import { PhotoManifest } from 'src/app/interfaces/manifests';

@Component({
  selector: 'app-rovers',
  templateUrl: './rovers.component.html',
  styleUrls: ['./rovers.component.scss'],
})
export class RoversComponent implements OnInit {
  rovers: Rover[] = [];

  constructor(private roversService: RoversService) {}

  ngOnInit(): void {
    this.roversService.rovers$.subscribe((rovers) => {
      this.rovers = rovers;
    });
  }
}
