import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Bookmark } from '../interfaces/bookmark';

@Injectable({
  providedIn: 'root',
})
export class BookmarksService {
  private _localStorage: Storage;

  private _bookmarks$ = new BehaviorSubject<Bookmark[]>([]);
  public bookmarks$ = this._bookmarks$.asObservable();

  constructor() {
    this._localStorage = localStorage;
    this.loadInfo();
  }

  setInfo(bookmark: Bookmark): boolean {
    const jsonData = JSON.stringify([...this._bookmarks$.value, bookmark]);
    try {
      this._localStorage.setItem('bookmarks', jsonData);
      this._bookmarks$.next([...this._bookmarks$.value, bookmark]);
      return true;
    } catch (e) {
      return false;
    }
  }

  loadInfo() {
    const data = JSON.parse(this._localStorage.getItem('bookmarks') || '[]');
    this._bookmarks$.next(data);
  }

  clearInfo() {
    this._localStorage.removeItem('bookmarks');
    this._bookmarks$.next([]);
  }

  clearAllLocalStorage() {
    this._localStorage.clear();
    this._bookmarks$.next([]);
  }
}
