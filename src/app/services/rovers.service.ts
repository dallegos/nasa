import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Photo } from '../interfaces/photo';
import { ManifestResult, PhotoResult, RoverResult } from '../interfaces/result';
import { Rover } from '../interfaces/rover';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class RoversService {
  private paginationLimit: number = 25;

  private _rovers$ = new BehaviorSubject<Rover[]>([]);
  public rovers$ = this._rovers$.asObservable();

  private _photos$ = new BehaviorSubject<Photo[]>([]);
  public photos$ = this._photos$.asObservable();

  constructor(private http: HttpClient, private router: Router) {
    this.getRoversFromApi();
  }

  getRover(roverSlug: string) {
    return this.rovers$.pipe(
      map((rovers) => rovers.find((rover) => rover.slug == roverSlug))
    );
  }

  getRoversFromApi() {
    return this.http
      .get<RoverResult>(
        `${environment.baseUrl}${environment.roversUrl}?api_key=${environment.apiKey}`
      )
      .subscribe({
        next: (result) => {
          this.mergeData(result).then((rovers) => {
            this._rovers$.next(rovers);
          });
        },
        error: (e: HttpErrorResponse) => this.handleError(e),
      });
  }

  setPhotos(photos: Photo[], merge: boolean = true) {
    if (merge) {
      this._photos$.next([...this._photos$.value, ...photos]);
      return;
    }

    this._photos$.next(photos);
  }

  async mergeData(result: RoverResult) {
    return Promise.all(
      result.rovers.map(async (rover) => {
        const manifest = await this.getManifest(rover.name.toLowerCase());
        return {
          ...rover,
          slug: rover.name.toLowerCase(),
          manifest,
        };
      })
    );
  }

  async getManifest(roverSlug: string) {
    if (!roverSlug) return;
    const manifest = await this.http
      .get<ManifestResult>(
        `${environment.baseUrl}${environment.manifestUrl}/${roverSlug}?api_key=${environment.apiKey}`
      )
      .toPromise();

    return manifest?.photo_manifest;
  }

  async getPhotosFromApi({
    rover,
    earth_date,
    sol_date,
    camera,
    page,
    resetPagination,
  }: {
    rover: string;
    earth_date?: string | null;
    sol_date?: number | null;
    camera?: string;
    page: number;
    resetPagination: boolean;
  }): Promise<boolean> {
    return new Promise(async (resolve) => {
      if (!rover) return resolve(false);

      let params = new HttpParams()
        .set('api_key', environment.apiKey)
        .set('page', page);

      if (!earth_date && !sol_date) {
        params = params.set('earth_date', this.formatDate(new Date()));
      }

      if (earth_date) {
        var [year, month, day] = earth_date.split('-');

        const newDate = new Date(
          Number.parseInt(year),
          Number.parseInt(month) - 1,
          Number.parseInt(day)
        );

        params = params.set('earth_date', this.formatDate(newDate));
      }

      if (sol_date) {
        params = params.set('sol', sol_date.toString());
      }

      if (camera) {
        params = params.set('camera', camera.toLowerCase());
      }

      const results = await this.http
        .get<PhotoResult>(
          `${environment.baseUrl}${environment.roversUrl}/${rover}/photos`,
          { params }
        )
        .toPromise();

      if (results && results.photos.length) {
        this.setPhotos(results.photos, !resetPagination);

        if (results.photos.length < this.paginationLimit) {
          return resolve(false);
        } else {
          return resolve(true);
        }
      } else {
        return resolve(false);
      }
    });
  }

  private formatDate(date: Date): string {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }

  private handleError(error: HttpErrorResponse) {
    console.error('An error occurred:', error.error);
    this.router.navigate(['/404']);
  }
}
